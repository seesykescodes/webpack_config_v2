const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const path = require("path");

module.exports = {
  context: path.resolve(__dirname, "src"),
  externals: ["foundation-sites"],
  entry: ["./app.js", "./index.html", "./settings.html"],
  output: {
    filename: "dist/bundle.js"
  },
  module: {
    rules: [
      /*
      your other rules for JavaScript transpiling go in here
      */
      {
        // css / sass / scss loader for webpack
        test: /\.(css|sass|scss)$/,
        use: ExtractTextPlugin.extract({
          use: ["css-loader", "sass-loader"]
        })
      },
      { test: /\.html$/, use: ["html-loader"] },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: { name: "[name].[ext]", outputPath: "/dist/assets/media" }
          }
        ]
      },
      // file-loader(for fonts)
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "url-loader?limit=100000",
            options: { name: "[name].[ext]", outputPath: "./dist/assets/fonts" }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new ExtractTextPlugin({
      // define where to save the file
      filename: "./dist/assets/css/[name].css",
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: "My Awesome application",
      myPageHeader: "index",
      template: "./index.html",
      chunks: ["vendor", "app"],
      filename: "./dist/index.html" //relative to root of the application
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: "My Awesome application",
      myPageHeader: "index",
      template: "./products.html",
      chunks: ["vendor", "app"],
      filename: "./dist/products.html" //relative to root of the application
    })
  ],
  devServer: {
    contentBase: "src",
    compress: true,
    port: 3000,
    stats: "errors-only",
    open: true
  }
};
