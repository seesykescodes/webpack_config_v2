
# Hi5 Webpack configuration

## Credit
[modified from the following repo](https://github.com/JonathanMH/webpack-scss-sass-file)

## Features
_________
* A ready-to-use project with Foundation (and all its dependencies) included.
* Hot reloading of Saas, media assets, HTML, and Javascript
* Ability to build multiple static files (with some light configuration) if needed

## Plugins and loaders
_________
* clean-webpack-plugin
* extract-text-webpack-plugin
* file-loader
* html-loader
* html-webpack-loader
* html-webpack-plugin
* css-loader
* url-loader
* webpack-dev-server

## Setup
_________
* Clone or download from bitbucket repo
* From root directory of project in Terminal, run "npm i" or "npm install"

## Available scripts
_________

    npm run build/npm run build-dev - runs webpack build process
    npm run watch - watch mode
    npm run dev - webpack dev server and hot reloading

## Directory layout
_________
```
├── dist (where "npm run build" outputs to)
├── src (where your source files should live)
│   ├── app.js
│   ├── assets
│   │   ├── media (any static images should be saved here)
│   │   └── scss (sass files live here)
│   │       ├── _initial.scss
│   │       └── app.scss
│   └── index.html
├── package.json
├── README.md
└── webpack.config.js (webpack configuration file)
```

## Notes

### Basic usage

you must add the following to any HTML file in order to setup hot reloading. this should be before the closing body tag to ensure of no DOM related issues
```html
<script src="/dist/bundle.js"></script>
```

Any css files should be included at the top of your HTML file.
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../dist/assets/css/main.css">
    <title>Hi5.agency</title>
</head>

<body>
  <!-- content goes here -->
    <script src="/dist/bundle.js"></script>
</body>

</html>

```
### Configuration for multiple static html files

You must make the following changes in ```webpack.config.js``` in order to make this work. This should be per static file. 

under ```entry```
```js
entry: ["./app.js", "./index.html", //./about.html //new file name with relative path from src here as an exampl]
```

under ```plugins```
```js
    new HtmlWebpackPlugin({
      hash: true,
      title: "My Awesome application", //Title of application
      myPageHeader: "index", //should match whatever page name you give
      template: "./index.html", //path to html file relative to src
      chunks: ["vendor", "app"], //chunks used for build.
      filename: "./dist/index.html" //relative to src folder, controls where build outputs to...
    }),

```

Here's a example of a ```webpack.config.js``` with multiple static entry points and outputs

```js
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const path = require("path");

module.exports = {
  context: path.resolve(__dirname, "src"),
  externals: ["foundation-sites"],
  entry: ["./app.js", "./index.html", "./settings.html"],
  output: {
    filename: "dist/bundle.js"
  },
  module: {
    rules: [
      /*
      your other rules for JavaScript transpiling (not included in build...) go in here
      */
      {
        // css / sass / scss loader for webpack
        test: /\.(css|sass|scss)$/,
        use: ExtractTextPlugin.extract({
          use: ["css-loader", "sass-loader"]
        })
      },
      { test: /\.html$/, use: ["html-loader"] },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: { name: "[name].[ext]", outputPath: "/dist/assets/media" }
          }
        ]
      },
      // file-loader(for fonts)
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "url-loader?limit=100000",
            options: { name: "[name].[ext]", outputPath: "./dist/assets/fonts" }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new ExtractTextPlugin({
      // define where to save the file
      filename: "./dist/assets/css/[name].css",
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: "My Awesome application",
      myPageHeader: "index",
      template: "./index.html",
      chunks: ["vendor", "app"],
      filename: "./dist/index.html" //relative to root of the application
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: "My Awesome application",
      myPageHeader: "index",
      template: "./products.html",
      chunks: ["vendor", "app"],
      filename: "./dist/products.html" //relative to root of the application
    })
  ],
  devServer: {
    contentBase: "src",
    compress: true,
    port: 3000,
    stats: "errors-only",
    open: true
  }
};

```

## Something's not working and this documentation sucks, who do I ask?
Johnathon or Rudy.